"""ppw_story1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from ppw_story4 import views as ppws4Views
from ppw_story import views as ppwViews
from django.views.generic.base import RedirectView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^kegiatan/', include('kegiatan.urls', namespace='kegiatan')),
    url(r'^home/$', RedirectView.as_view(url='/')),
    url(r'^matkul/', include('mata_kuliah.urls', namespace='matkul')),
    url('admin/', admin.site.urls),
    url(r'^profiles1/$', ppwViews.profiles1),
    url(r'^profile/$', ppws4Views.profile),
    url(r'^myself/$', ppws4Views.myself),
    url(r'^$', ppws4Views.home),
]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
