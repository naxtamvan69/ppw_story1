from django.shortcuts import render

def home(request):
    return render(request, 'Homepage.html')

def myself(request):
    return render(request, 'MySelf.html')

def profile(request):
    return render(request, 'Profile.html')
