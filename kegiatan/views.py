from django.shortcuts import render,redirect
from .models import Kegiatan, Human
from .forms import human
# Create your views here.

def listKegiatan(request):
    kegiatan = Kegiatan.objects.all()
    context = {
        'kegiatan': kegiatan
    }
    return render(request, 'kegiatan/listKegiatan.html', context)

def detaiKegiatan(request, id):
    kegiatan = Kegiatan.objects.get(id=id)
    human = Human.objects.filter(kegiatan__id = id)
    contects = {
        'kegiatan': kegiatan,
        'humans': human,
    }
    return render(request, 'kegiatan/detailKegiatan.html', contects)

def addHuman(request):
    human_form = human(request.POST or None)
    context = {
        'human_form': human_form,
    }
    if request.method == 'POST':
        if human_form.is_valid():
            human_form.save()
            return redirect('kegiatan:home')
    return render(request, 'kegiatan/addHuman.html', context)
