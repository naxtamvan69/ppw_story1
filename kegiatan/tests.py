from django.test import TestCase, Client
from .models import Kegiatan, Human

# Create your tests here.

class Test(TestCase):
    def test_url_list_Kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_template_list_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/listKegiatan.html')

    def test_model_kegiatan(self):
        Kegiatan.objects.create(name_kegiatan="INI KEGIATAN", description="PPW IS FUN")
        count = Kegiatan.objects.all().count()
        self.assertEqual(count, 1)

    def test_url_add_human(self):
        response = Client().get('/kegiatan/addhuman/')
        self.assertEqual(response.status_code, 200)

    def test_template_add_human(self):
        response = Client().get('/kegiatan/addhuman/')
        self.assertTemplateUsed(response, 'kegiatan/addHuman.html')

    def test_view_list_kegiatan(self):
        response = Client().get('/kegiatan/')
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn("List Kegiatan", isi_html_kembalian)

    def test_view_add_human(self):
        response = Client().get('/kegiatan/addhuman/')
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('Tambah Peserta', isi_html_kembalian)
        self.assertIn('Nama', isi_html_kembalian)
        self.assertIn('Kegiatan', isi_html_kembalian)
        self.assertIn('<input type="text" name="name_human" class="form-control" id="exampleFormControlInput1" placeholder="contoh: Pak Pewe" maxlength="50" required>',
            isi_html_kembalian)

    def test_model_human(self):
        Human.objects.create(name_human="Asep")
        count = Human.objects.all().count()
        self.assertEqual(count, 1)