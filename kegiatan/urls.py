from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<id>[\d]+)/$', views.detaiKegiatan, name='detailed'),
    url(r'^$', views.listKegiatan, name='home'),
    url(r'^addhuman/$', views.addHuman, name='addhuman')
]

app_name = 'kegiatan'