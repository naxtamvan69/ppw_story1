# Generated by Django 3.1.2 on 2020-10-24 06:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kegiatan', '0002_auto_20201024_1216'),
    ]

    operations = [
        migrations.AddField(
            model_name='kegiatan',
            name='image',
            field=models.ImageField(default=2, upload_to=''),
            preserve_default=False,
        ),
    ]
