from django.db import models
from django.utils import timezone

class Kegiatan(models.Model):
    name_kegiatan = models.CharField(max_length=50)
    description = models.TextField(max_length=250)
    image = models.ImageField(null=True, blank=True, upload_to='images/')

    def __str__(self):
        return '{}'.format(self.name_kegiatan)

class Human(models.Model):
    name_human = models.CharField(max_length=50)
    kegiatan = models.ManyToManyField(Kegiatan)
    time = models.TimeField(default= timezone.now)

    def __str__(self):
        return '{}'.format(self.name_human)