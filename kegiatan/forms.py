from django import forms
from . import models

class human(forms.ModelForm):
    class Meta:
        model = models.Human
        fields = [
            'name_human',
            'kegiatan',
        ]

        labels = {
            'name_human': 'Nama',
            'kegiatan': 'Kegiatan',
        }

        widgets = {
            'name_human': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': "exampleFormControlInput1",
                    'placeholder': "contoh: Pak Pewe",
                }
            ),
        }
