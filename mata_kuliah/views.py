from django.shortcuts import render, redirect
from .models import Post

# Create your views here.
from .forms import mataKuliah
from django.utils import timezone

def addMatkul(request):
    matkul_form = mataKuliah(request.POST or None)
    context = {
        'data_form': matkul_form,
    }
    if request.method == 'POST':
        if matkul_form.is_valid():
            matkul_form.save()
            return redirect('matkul:home')
    return render(request, 'mata_kuliah/addmatkul.html', context)

def homeMatkul(request):
    list_matkul = Post.objects.all()
    context = {
        'list_matkul': list_matkul
    }
    return render(request, 'mata_kuliah/homematkul.html', context)

def deleteMatkul(request):
    list_matkul = Post.objects.all()
    context = {
        'list_matkul': list_matkul
    }
    return render(request, 'mata_kuliah/removematkul.html', context)

def detailedMatkul(request, id):
    detail = None
    post_list = Post.objects.filter(id=id)
    if len(post_list) > 0:
        detail = post_list[0]
    else:
        detail = None
    contects = {
        'detail': detail,
        'title' : 'Matkul: ' + detail.nameMatkul
    }
    return render(request, 'mata_kuliah/detailedmatkul.html',contects)

def delete(request, delete_id):
    Post.objects.get(id=delete_id).delete()
    return redirect('matkul:remove')
