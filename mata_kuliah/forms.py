from django import forms
from .models import Post

class mataKuliah(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            'nameMatkul',
            'teacher',
            'sks',
            'desc',
            'term',
            'classs',
        ]

        labels = {
            'nameMatkul':'Nama Mata Kuliah',
            'teacher' : 'Nama Dosen',
            'sks' : 'Jumlah SKS',
            'desc' : 'Deskripsi',
            'term' : 'Tahun Ajaran',
            'classs' : 'Ruangan',
        }

        widgets = {
            'teacher' : forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id' : "exampleFormControlInput1",
                    'placeholder' : "contoh: Pak Pewe",
                }
            ),
            'sks': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': "exampleFormControlInput1",
                    'placeholder': "contoh: 3",
                }
            ),
            'desc': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'id': "exampleFormControlTextarea1",
                    'placeholder': "contoh: PPW is Fun",
                }
            ),
            'term': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': "exampleFormControlInput1",
                    'placeholder': "contoh: Gasal (2020/2021)",
                }
            ),
            'nameMatkul': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': "exampleFormControlInput1",
                    'placeholder': "contoh: Perancangan dan Pemrograman Web",
                }
            ),
            'classs': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': "exampleFormControlInput1",
                    'placeholder': "contoh: 4.3104",
                }
            )
        }
