from django.db import models
from django.utils import timezone
from .validators import validate_sks

# Create your models here.

class Post(models.Model):
    nameMatkul = models.CharField(max_length= 50)
    teacher = models.CharField(max_length=25)
    sks = models.CharField(max_length=2,
                           validators= [validate_sks])
    desc = models.TextField(max_length=250)
    term = models.CharField(max_length=20)
    classs = models.CharField(max_length=7)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{}".format(self.nameMatkul)


