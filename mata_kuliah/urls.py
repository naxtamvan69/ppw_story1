from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^remove/(?P<delete_id>[0-9]+)$', views.delete, name='delete'),
    url(r'^remove/$', views.deleteMatkul, name='remove'),
    url(r'^add/$', views.addMatkul),
    url(r'^$', views.homeMatkul, name='home'),
    url(r'^(?P<id>[\d]+)/$', views.detailedMatkul, name='detailed'),
]

app_name = 'matkul'