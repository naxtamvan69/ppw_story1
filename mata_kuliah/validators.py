from django.core.exceptions import ValidationError


def validate_sks(value):
    sks = value
    try:
        int(sks)
    except ValueError:
        message = ('maaf {} bukan integer'.format(sks))
        raise ValidationError(message)