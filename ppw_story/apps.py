from django.apps import AppConfig


class PpwStoryConfig(AppConfig):
    name = 'ppw_story'
